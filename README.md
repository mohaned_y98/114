<p align="center">
  <a href="https://flutter.io/">
    <img src="https://i.imgur.com/JsAX4df.png" alt="Logo" width=250 height=250>
  </a>

  <h3 align="center">114</h3>

  <p align="center">
    Holy Quran Search App
    <br>
    Searching By Word and getting the Surah & the Ayah where the word is located and how many times the word is repeated in quran.
    <br>
    <br>
    <a href="https://github.com/Mohanedy98/114/issues/new">Report bug</a>
    ·
    <a href="https://github.com/Mohanedy98/114/issues/new">Request feature</a>
<br>

[![made-with-python](https://img.shields.io/badge/Made%20with-flutter-1f425f.svg)](https://flutter.dev/)
[![Open Source Love svg1](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://github.com/Mohanedy98)

  </p>
</p>


## Table of contents

- [Quick start](#quick-start)
- [Screenshots](#screenshots)
- [Built With](#built-with)
- [Features](#features)
- [Creator](#creator)
- [Thanks](#thanks)
- [Copyright and license](#copyright-and-license)

## Quick start

This app is for **Searching** By **Word** and getting the **Surah & the Ayah where the word is located** and how many times the word is **repeated** in quran.

## Screenshots
![114 App in use](https://i.imgur.com/XMPUwA7.gif)

## Built With
* Flutter 1.5.4
* Dart 2.3.0

## Features

* Work's Offline (No online API used)
* Arabic Language
* Compatible with both Android & iOS
* Responsive layout
* Follows Material Design Guidelines
* Follows OOP practice


## Creator

[**Mohaned Yossry**](https://github.com/Mohanedy98)

## Thanks

Thanks to [semarketir](https://github.com/semarketir/quranjson) for Providing Quran in JSON Format

## Copyright and license

Code and documentation copyright 2019 the authors. Code released under the MIT License.

Enjoy
